#!/bin/bash
/usr/bin/python3 face_mask_detection_int8.py --model models/facemask_model_edgetpu.tflite --input $1 --label ./models/facemask_labels.txt
