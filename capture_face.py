from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from dotenv import load_dotenv
load_dotenv(verbose=True)

import argparse
import io
import os
import time
import numpy as np
import picamera
from shutil import copyfile

from PIL import Image

import classify
from edgetpu.detection.engine import DetectionEngine
from edgetpu.utils import dataset_utils
import tflite_runtime.interpreter as tflite
from PIL import ImageDraw
import platform

EDGETPU_SHARED_LIB = {
  'Linux': 'libedgetpu.so.1',
  'Darwin': 'libedgetpu.1.dylib',
  'Windows': 'edgetpu.dll'
}[platform.system()]

HUMAN_PRESENCE_FILE = os.getenv("HUMAN_PRESENCE_FILE")
PROJ_DIR = os.getenv("PROJ_DIR")
FACE_MASK_FILE = os.getenv("FACE_MASK_FILE")
RESULT_IMAGE = os.getenv("RESULT_IMAGE")

def load_labels(path, encoding='utf-8'):
  """Loads labels from file (with or without index numbers).
  Args:
    path: path to label file.
    encoding: label file encoding.
  Returns:
    Dictionary mapping indices to labels.
  """
  with open(path, 'r', encoding=encoding) as f:
    lines = f.readlines()
    if not lines:
      return {}

    if lines[0].split(' ', maxsplit=1)[0].isdigit():
      pairs = [line.split(' ', maxsplit=1) for line in lines]
      return {int(index): label.strip() for index, label in pairs}
    else:
      return {index: line.strip() for index, line in enumerate(lines)}

def make_interpreter(model_file):
  model_file, *device = model_file.split('@')
  return tflite.Interpreter(
      model_path=model_file,
      experimental_delegates=[
          tflite.load_delegate(EDGETPU_SHARED_LIB,
                               {'device': device[0]} if device else {})
      ])

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('--model', required=True,
      help='Detection SSD model path (must have post-processing operator).')
  parser.add_argument(
      '-fmm', '--fmmodel', required=True, help='File path of .tflite file.')    
  parser.add_argument('--label', help='Labels file path.')
  parser.add_argument('--fmlabel', help='Face mask labels file path.')
  parser.add_argument('--input', help='Input image path.', required=True)
  parser.add_argument('--output', help='Output image path.')
  parser.add_argument('-k', '--top_k', type=int, default=1, help='Max number of classification results')
  parser.add_argument('-t', '--threshold', type=float, default=0.0, help='Classification score threshold')
  parser.add_argument('-c', '--count', type=int, default=5, help='Number of times to run inference')
  parser.add_argument('--keep_aspect_ratio', action='store_true',
      help=(
          'keep the image aspect ratio when down-sampling the image by adding '
          'black pixel padding (zeros) on bottom or right. '
          'By default the image is resized and reshaped without cropping. This '
          'option should be the same as what is applied on input images during '
          'model training. Otherwise the accuracy may be affected and the '
          'bounding box of detection result may be stretched.'))
  args = parser.parse_args()
  # Initialize engine.
  engine = DetectionEngine(args.model)
  labels = dataset_utils.read_label_file(args.label) if args.label else None

  with picamera.PiCamera(resolution=(640, 480), framerate=30) as camera:
    camera.start_preview()
    try:
      stream = io.BytesIO()
      for _ in camera.capture_continuous(
          stream, format='jpeg', use_video_port=True):
        stream.seek(0)
        image = Image.open(stream).convert('RGB').resize((640, 480),
                                                         Image.ANTIALIAS)
        start_time = time.time()
        image.save(args.input)
        img = Image.open(args.input).convert('RGB')
        draw = ImageDraw.Draw(img)

        # Run inference.
        objs = engine.detect_with_image(img,
                                        threshold=0.05,
                                        keep_aspect_ratio=args.keep_aspect_ratio,
                                        relative_coord=False,
                                        top_k=10)
        detected_human = 0
        # Print and draw detected objects.
        print(objs)
        for obj in objs:
          print('-----------------------------------------')
          if labels:
            print(labels[obj.label_id])
          print('score ===> ', obj.score)
          if obj.score > 0.5:
            detected_human = 1
          box = obj.bounding_box.flatten().tolist()
          print('box =', box)
          draw.rectangle(box, outline='red')
          
        if not objs:
          print('No objects detected.')
          detected_human = 0
        
        print('---- HP ' + str(detected_human) + ' ----')
        file = open(PROJ_DIR + HUMAN_PRESENCE_FILE, 'w')
        file.write('%s' % (str(detected_human)))
        file.close()
          
        # Save image with bounding boxes.
        if args.output:
          img.save(args.output)
          destfilenamepath = PROJ_DIR + RESULT_IMAGE
          copyfile(args.output, destfilenamepath)

        face_mask_detect = 0

        if detected_human == 1:
          fmlabel = load_labels(args.fmlabel) if args.fmlabel else {}

          interpreter = make_interpreter(args.fmmodel)
          interpreter.allocate_tensors()

          size = classify.input_size(interpreter)
          print(args.input)
          image = Image.open(args.input).convert('RGB').resize(size, Image.ANTIALIAS)
          classify.set_input(interpreter, image)
          input_index = interpreter.get_input_details()[0]["index"]
          output_index = interpreter.get_output_details()[0]["index"]
          test_image = np.expand_dims(image, axis=0).astype(np.float32)
          max_value = np.max(test_image)
          test_image = test_image/(max_value/2) -1
          interpreter.set_tensor(input_index, test_image)
          interpreter.invoke()
          predictions = interpreter.get_tensor(output_index)

          print('----INFERENCE TIME----')
          print('Note: The first inference on Edge TPU is slow because it includes',
                'loading the model into Edge TPU memory.')
          for _ in range(args.count):
            start = time.perf_counter()
            interpreter.invoke()
            inference_time = time.perf_counter() - start
            classes = classify.get_output(interpreter, args.top_k, args.threshold)
            print('%.1fms' % (inference_time * 1000))
          print("predictions:",predictions)
          
          if predictions[0][0] > predictions[0][1]:
            print("Face mask detected")
            face_mask_detect = 1
          else: 
            print("No face mask detected")
          
        file = open(PROJ_DIR + FACE_MASK_FILE, 'w')
        file.write('%s' % (str(face_mask_detect)))
        file.close()

        elapsed_ms = (time.time() - start_time) * 1000
        stream.seek(0)
        stream.truncate()
    finally:
      camera.stop_preview()


if __name__ == '__main__':
  main()
