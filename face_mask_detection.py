from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from dotenv import load_dotenv
load_dotenv(verbose=True)

import argparse
import io
import os
import time
import numpy as np

from PIL import Image

from edgetpu.detection.engine import DetectionEngine
from edgetpu.utils import dataset_utils
from PIL import ImageDraw


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('--model', required=True,
      help='Detection SSD model path (must have post-processing operator).')
  parser.add_argument('--label', help='Labels file path.')
  parser.add_argument('--input', help='Input image path.', required=True)
  parser.add_argument('--output', help='Output image path.')
  parser.add_argument('--keep_aspect_ratio', action='store_true',
      help=(
          'keep the image aspect ratio when down-sampling the image by adding '
          'black pixel padding (zeros) on bottom or right. '
          'By default the image is resized and reshaped without cropping. This '
          'option should be the same as what is applied on input images during '
          'model training. Otherwise the accuracy may be affected and the '
          'bounding box of detection result may be stretched.'))
  args = parser.parse_args()
  # Initialize engine.
  engine = DetectionEngine(args.model)
  labels = dataset_utils.read_label_file(args.label) if args.label else None



if __name__ == '__main__':
  main()
