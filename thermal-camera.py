from dotenv import load_dotenv
load_dotenv(verbose=True)

import os
import sys
import math
import time
import datetime

import busio
import board
import random

import numpy as np
import adafruit_amg88xx
from PIL import Image, ImageDraw
from colour import Color
from shutil import copyfile
import uuid 
import pyrebase

# initialize the thermal cam on the i2c bus
i2c_bus = busio.I2C(board.SCL, board.SDA)
sensor = adafruit_amg88xx.AMG88XX(i2c_bus)

# low range temperature of the sensor (this will be blue on the screen)
MINTEMP = 27.

# high range temperature of the sensor (this will be red on the screen)
MAXTEMP = 39.1

# height and width of the AMG8822 sensor 
WIDTH = 8
HEIGHT = 8

#how many color values we can have
COLORDEPTH = 1024

# let the sensor initialize
time.sleep(.1)

#the list of colors we can choose from range
blue = Color("indigo")
colors = list(blue.range_to(Color("red"), COLORDEPTH))
 
#create the array of colors
colors = [(int(c.red * 255), int(c.green * 255), int(c.blue * 255)) for c in colors]

# reading all the require env variables 
TEMP_FILE = os.getenv("TEMP_FILE")
PROJ_DIR = os.getenv("PROJ_DIR")
AVG_TEMP_FILE = os.getenv("AVG_TEMP_FILE")
TEMP_IMAGE = os.getenv("TEMP_IMAGE")
RESULT_IMAGE = os.getenv("RESULT_IMAGE")
HUMAN_PRESENCE_FILE = os.getenv("HUMAN_PRESENCE_FILE")
STATIC_CAPTURE_DIR = os.getenv("STATIC_CAPTURE_DIR")
DEVICE_ID = os.getenv("DEVICE_ID")
FIREBASE_USER = os.getenv("FIREBASE_USER")
FIREBASE_USER_PASSWORD = os.getenv("FIREBASE_USER_PASSWORD")
BLINK_FEVER_FILE = os.getenv("BLINK_FEVER_FILE")

# init all the required credentials to connect to google firebase real time db
config = {
  "apiKey": os.getenv("FIREBASE_API_KEY"),
  "authDomain": os.getenv("FIREBASE_AUTH_DOMAIN"),
  "databaseURL": os.getenv("FIREBASE_DATABASE_URL"),
  "storageBucket": os.getenv("FIREBASE_STORAGE_BUCKET")
}

# initial firebase database 
firebase = pyrebase.initialize_app(config)
# Get a reference to the auth service
auth = firebase.auth()

# Log the user in , every device will have its own login credential
user = auth.sign_in_with_email_and_password(FIREBASE_USER, FIREBASE_USER_PASSWORD)

# Get a reference to the database service
db = firebase.database()

# Get firebase storage object 
storage = firebase.storage()

# decide which color to assign according to the given temperature from thermal camera sensor
def getTempColour(temp):
    v = int((temp - MINTEMP) * float(len(colors)-1) / (MAXTEMP - MINTEMP))
    y = MAXTEMP - temp
    if v > 255 or v < 0:
      v = 255
    print('---[==' + str(temp) + '==]---')
    if temp != None:
      if temp < MINTEMP:
          b = random.randrange(200, v)
          y2 = random.randrange(2, 50)
          g = y2 + y
          if b > 255:
              b = 255
          if g > 255:
              g = 255
          return (0, g,b)
      elif temp > MINTEMP and temp < MAXTEMP:
          y2 = random.randrange(2, 90)
          g = y2 + y
          if g > 255:
              g = 255
          return (v, g, 0)
      elif temp > MAXTEMP:
          return (random.randrange(220,v), abs(y), 0)
      else:
          y1 = random.randrange(2, 31)
          y2 = random.randrange(242, 255)
          
          return (0, y1, y2)
    else:
      y1 = random.randrange(2, 32)
      y2 = random.randrange(253, 255)
      return (0, y1, y2)

# read the matrix temperature values from the sensor and 
# create a thermal image based on the colour assigned to the temp threshold
def captureThermalCamera():
    while True:
        file = open(PROJ_DIR + TEMP_FILE, "r")
        values = file.read()
        tempArr = values.split('|')
        MINTEMP = float(tempArr[0]) if tempArr[0] else 27.
        MAXTEMP = float(tempArr[1]) if tempArr[1] else 36.1
        
        # read the pixels from the sensor
        pixels = []
        for row in sensor.pixels:
            pixels = pixels + row
        image = Image.new('RGBA', (WIDTH, HEIGHT))
        # conver the pixels array into numpy array
        newA = np.array(pixels)
        print(newA)
        # get the max temperature read from the sensor
        maxAverageTemp = np.amax(newA)
        print('[---- ' + str(maxAverageTemp) + ' ----]')
        tweakTemp = getTunedTemperature(maxAverageTemp)
        print('(---- ' + str(tweakTemp) + ' ----)')
        # write the average temperature to a data file
        file = open(PROJ_DIR + AVG_TEMP_FILE, 'w')
        file.write('%s' % (tweakTemp))
        file.close()
        counter = 0
        # start plotting the thermal image
        for x in range(WIDTH):
            for y in range(HEIGHT):
                if newA[counter] < 28:
                    colourT = getTempColour(newA[counter])
                else: 
                    colourT = getTempColour(getTunedTemperature(newA[counter]))
                image.putpixel((x, y), (int(colourT[0]), int(colourT[1]), int(colourT[2])))
                counter+= 1
        # https://en.wikipedia.org/wiki/Lanczos_resampling
        # Lanczos filtering and Lanczos resampling are two applications of a mathematical formula. 
        # It can be used as a low-pass filter or used to smoothly interpolate the value of 
        # a digital signal between its samples.
        newImage = image.resize((320, 240), Image.LANCZOS)
        # save the thermal image to a location
        newImage.save(PROJ_DIR + TEMP_IMAGE)
        time.sleep(.5)
        human_pre_file = open(PROJ_DIR + HUMAN_PRESENCE_FILE, "r")
        humanpreVal = human_pre_file.read()
    
        # check the average tweak temp against the system configure maximum temperature
        # if above the threshold save a record to the real time database
        if tweakTemp >= MAXTEMP:
            # check whether human is presence
            # strictly for human
            if humanpreVal == '1':
                file = open(PROJ_DIR + BLINK_FEVER_FILE, 'w')
                file.write('%s' % (1))
                file.close()
                if os.path.getsize(PROJ_DIR + RESULT_IMAGE) > 0:
                  tofilename = str(uuid.uuid1()) + '.png'
                  destfilenamepath = PROJ_DIR + STATIC_CAPTURE_DIR + tofilename
                  print(destfilenamepath)
                  print(PROJ_DIR + RESULT_IMAGE)
                  # copy the image generated from the pi camera to another location
                  copyfile(PROJ_DIR + RESULT_IMAGE, destfilenamepath)
                  storage.child('fever/' + tofilename).put(destfilenamepath, user['idToken'])
                  fileURL = storage.child('fever/' + tofilename).get_url(user['idToken'])
                  whattime = datetime.datetime.now()
                  # form a data structure to save
                  data = {
                    str(uuid.uuid1()): {  
                        "temperature": tweakTemp,
                        "photoURL" : fileURL,
                        "timestamp": str(whattime),
                        "deviceId": DEVICE_ID 
                    }
                  }
                  # Pass the fever data's idToken to the push method
                  results = db.child("user_fever").push(data, user['idToken'])
                  ## house keep the face detected image after save to firebase
                  if os.path.exists(destfilenamepath):
                    os.remove(destfilenamepath)
                  else:
                    print("The image file does not exist")
                  file = open(PROJ_DIR + BLINK_FEVER_FILE, 'w')
                  file.write('%s' % (0))
                  file.close()
        else:
            if humanpreVal == '1':
              file = open(PROJ_DIR + BLINK_FEVER_FILE, 'w')
              file.write('%s' % (2))
              file.close()
            else:
              file = open(PROJ_DIR + BLINK_FEVER_FILE, 'w')
              file.write('%s' % (0))
              file.close()
    

# tweak the temperature reading (this implementation need improvement)
# TODO implement future 3 models- normal , cold and hot https://www.researchgate.net/publication/329648882_Temperature_Model_and_Human_Detection_in_Thermal_Image 
# https://www.controleng.com/articles/tuning-thermal-pid-loops/
# sorry for the quick hack here ! will fix it once I firm out the solution.
def getTunedTemperature(temp):
    resultTemp = temp
    if temp < 27:
        resultTemp = 35.9
    elif temp >= 27 and temp <= 28: 
        resultTemp = 36.2    
    elif temp > 28 and temp <= 31: 
        resultTemp = 36.9
    elif temp > 31 and temp < 37.4:
        resultTemp = 37.2
    if resultTemp >= 39:
        resultTemp = 39
    return resultTemp

if __name__ == "__main__":
 try:
  captureThermalCamera()
 except KeyboardInterrupt:
  print("thermal camera program exited!")
