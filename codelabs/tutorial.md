author:            Kenneth Phang Tak Yan
summary:           Create a prototype thermal camera solution using Python on a Raspberry Pi
id:                29101
categories:        python
environments:      iot
status:            draft
feedback link:     github.com/kenken64
analytics account: 0

# Create a prototype thermal camera solution using Python on a Raspberry Pi

## Overview of the tutorial
Duration: 0:05

This tutorial shows you how to create a prototype thermal camera solution using Python on a Raspberry Pi: 

* Knowledge on how to build a DIY thermal camera 
* Hardware required to make this prototype
* Understand how to retrieve temperature values
* Generate and stream the thermal image as MJPEG
* Implement face detection and stream the person’s face 
(Edge computing)
* Build a Simple Web App (Replace the LCD screen)
* Save sensor data to the cloud server (Firebase)

Prerequesites

* Python
* Google Firebase
* Raspbian Lite 

We recommend you to have Python and Raspian already installed on your Raspberry Pi 4.

## Slides
Duration: 35:0

<img src="./images/slides1.jpg"/>

[Click here to view Slides](https://drive.google.com/file/d/1cp9LQnVJ3P0wItfJrb1pjJuzf7mq7BgK/view?usp=sharing)

## Required hardware 
Duration: 15:0

* [Raspberry Pi 4 4GB RAM](https://www.sgbotic.com/index.php?dispatch=products.view&product_id=2962)
* [16GB MicroSD card](https://www.sgbotic.com/index.php?dispatch=products.view&product_id=2987)
* [Adafruit AMG8833 Thermal Camera 8x8](https://www.adafruit.com/product/3538)
* [Jumper cable](https://www.sgbotic.com/index.php?dispatch=products.view&product_id=1910)
* [Grove connector cable](http://amicus.com.sg/migration_prd/shop/grove/cable/grove-universal-4-pin-buckled-20cm-cable-5-pcs-pack/)
* [Raspberry Pi Camera v2](http://amicus.com.sg/migration_prd/shop/raspberry-pi-2/raspberry-pi-camera-v2-camera-module/)
* [Google Coral USB - Google Edge TPU coprocessor](https://coral.ai/products/accelerator/)
* [WS2812 Addressable RGB LED Stick](https://www.sgbotic.com/index.php?dispatch=products.view&product_id=1928)


## Wiring Diagram
Duration: 20:0



