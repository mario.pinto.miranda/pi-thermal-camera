from dotenv import load_dotenv
load_dotenv(verbose=True)

from flask import Flask, render_template, request, Response, jsonify, abort
from flask_compress import Compress
from flask_cors import CORS
from PIL import Image
import base64
import io
import os
import subprocess
from functools import wraps

app = Flask(__name__)
Compress(app)
CORS(app)

TEMP_FILE = os.getenv("TEMP_FILE")
PROJ_DIR = os.getenv("PROJ_DIR")
AVG_TEMP_FILE = os.getenv("AVG_TEMP_FILE")
TEMP_IMAGE = os.getenv("TEMP_IMAGE")
RESULT_IMAGE = os.getenv("RESULT_IMAGE")
HUMAN_PRESENCE_FILE = os.getenv("HUMAN_PRESENCE_FILE")
TEMP_CONFIGURE = os.getenv("TEMP_CONFIGURE")
FACE_MASK_FILE = os.getenv("FACE_MASK_FILE")
DEVICE_ID = os.getenv("DEVICE_ID")

address = subprocess.check_output(['hostname', '-s', '-I'])
address = address.decode('utf-8') 
IPAddr=address[:-1]

# load landing page with the image streaming canvas
@app.route("/")
def index():
    return render_template("index.html",defaultMin=26,defaultMax=36.1,ipAddress=IPAddr.strip())


# save the min and max temperature from the client side
@app.route('/configure-temp', methods=['POST']) #allow POST requests
def configureTemperature():
    if request.method == 'POST':
        minTempRange = request.form.get('minTempRange')
        maxTempRange = request.form['maxTempRange']
        print('%s - %s' % (minTempRange, maxTempRange))
        temp = {
          'minTempRange': minTempRange,
          'maxTempRange': maxTempRange
        }
        file = open(PROJ_DIR + TEMP_FILE, 'w')
        file.write('%s|%s' % (minTempRange, maxTempRange))
        file.close()
        return jsonify(temp)

# get the saved min and max temperature
@app.route('/temperature', methods=['GET'])
def getConfigureTemperature():
  human_pre_file = open(PROJ_DIR + HUMAN_PRESENCE_FILE, "r")
  humanpreVal = human_pre_file.read()
  file = open(PROJ_DIR + TEMP_FILE, "r")
  values = file.read()
  tempArr = values.split('|')
  fileAvg = open(PROJ_DIR + AVG_TEMP_FILE, "r")
  valuesAvg = fileAvg.read()
  facemaskInd = open(PROJ_DIR + FACE_MASK_FILE, "r")
  facemaskInd = facemaskInd.read()
  temp = {
    'minTempRange': tempArr[0],
    'maxTempRange': tempArr[1],
    'avgTemp': round(float(valuesAvg),2),
    "humanPresence": humanpreVal,
    "tempConfigure": TEMP_CONFIGURE,
    "faceMaskInd": facemaskInd
  }
  return jsonify(temp)

# route that provide a base64 encoded image string to the frontend
@app.route("/thermal-img", methods=['GET'])
def thermalImg():
    return getImageFromPathRetBase64(TEMP_IMAGE)

# route that provide a base64 encoded image string to the frontend
@app.route("/person-img", methods=['GET'])
def personImg():
    return getImageFromPathRetBase64(RESULT_IMAGE)

def getImageFromPathRetBase64(imagePath):
    try:
      im = Image.open(PROJ_DIR + imagePath, mode='r')
      img_byte_arr = io.BytesIO()
      im.save(img_byte_arr, format='PNG')
      encoded_img = base64.encodebytes(img_byte_arr.getvalue()).decode('ascii')
      return 'data:image/png;base64, %s' % encoded_img
    except OSError:
      print("Image not ready yet")
      return Response('image not found', 404, {})
    except TypeError:
      print('Type error from view ')
      return Response('Type error', 404, {})

if __name__ == "__main__":
    app.run()
